import array from "../testData.js";
import map from "../map.js";

function square(value) {
  return value * value;
}

console.log(map(array, square));
