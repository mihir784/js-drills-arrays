function filter(elements, cb) {
  let filteredArray = [];
  for (let i = 0; i < elements.length; i++) {
    let current = elements[i];
    if (cb(current)) {
      filteredArray.push(current);
    }
  }
  return filteredArray;
}

export default filter;
