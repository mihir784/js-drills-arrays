import array from "../testData.js";
import find from "../find.js";

function cb(element) {
  return element == 5;
}

console.log(find(array, cb));
