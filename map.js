function map(elements, cb) {
  let mappedArray = [];
  for (let i = 0; i < elements.length; i++) {
    mappedArray.push(cb(elements[i]));
  }
  return mappedArray;
}

export default map;
