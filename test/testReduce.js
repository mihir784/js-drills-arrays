import array from "../testData.js";
import reduce from "../reduce.js";

function sum(result, currValue) {
  return result + currValue;
}

console.log(reduce(array, sum, 0));
