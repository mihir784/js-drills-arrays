import flatten from "../flatten.js";

let nestedArray = [1, [2], [[3]], [[[4]]]];

console.log(flatten(nestedArray));
