function find(elements, cb) {
  for (let i = 0; i < elements.length; i++) {
    let current = elements[i];

    if (cb(current)) {
      return current;
    }
  }
  return;
}

export default find;
