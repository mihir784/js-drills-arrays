function reduce(elements, cb, startingValue = 0) {
  let result = startingValue;

  for (let i = 0; i < elements.length; i++) {
    result = cb(result, elements[i]);
  }

  return result;
}

export default reduce;
