function flatten(array) {
  let flattenArray = [];

  for (let i = 0; i < array.length; i++) {
    let current = array[i];

    if (Array.isArray(current)) {
      flattenArray = flattenArray.concat(flatten(current));
    } else {
      flattenArray.push(current);
    }
  }
  
  return flattenArray;
}

export default flatten;
